import Vue from 'vue'
import VueRouter from 'vue-router'

import Orders from './components/Orders.vue'
import Services from './components/Services.vue'
import Managers from './components/Managers.vue'
import Clients from './components/Clients.vue'
import SalesRepost from './components/SalesRepost.vue'
import Start from './components/Start.vue'
import Calendar from './components/Calendar.vue'
import Advertising from './components/Advertising.vue'

Vue.use(VueRouter);


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Start',
            label: 'Начало работы',
            component: Start
        },
        {
            path: '/orders',
            name: 'orders',
            label: 'Заказы',
            component: Orders
        },
        {
            path: '/services',
            name: 'services',
            label: 'Услуги',
            component: Services
        },
        {
            path: '/managers',
            name: 'managers',
            label: 'Управление персоналом',
            component: Managers
        },
        {
            path: '/clients',
            name: 'clients',
            label: 'Клиенты',
            component: Clients
        },
        {
            path: '/sales-repost',
            name: 'sales-repost',
            label: 'Отчёт по продажам',
            component: SalesRepost
        },
        {
            path: '/calendar',
            name: 'Calendar',
            label: 'Календарь задач',
            component: Calendar
        },
        {
            path: '/advertising',
            name: 'Advertising',
            label: 'Рассылки и реклама',
            component: Advertising
        }
    ]
});

export default router;


