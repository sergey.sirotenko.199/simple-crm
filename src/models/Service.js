var Service = function(options) {
    options.id              ? this.id = options.id                          : this.id = null;
    options.serviceName     ? this.serviceName = options.serviceName        : this.serviceName = '';
    options.description     ? this.description = options.description        : this.description = '';
    options.price           ? this.price = options.price                    : this.price = 0;
    options.motivation      ? this.motivation = options.motivation          : this.motivation = 0;
}
export default Service;