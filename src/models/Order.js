var Order = function(options) {
    options.id              ? this.id = options.id                          : this.id = null;
    options.clientName      ? this.clientName = options.clientName          : this.clientName = '';
    options.orderPositions  ? this.orderPositions = options.orderPositions  : this.orderPositions = [];
    options.finishdate      ? this.finishdate = options.finishdate          : this.finishdate = null;
    options.comment         ? this.comment = options.comment                : this.comment = '';
}
export default Order;